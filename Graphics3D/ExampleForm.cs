﻿using Graphics3D.Graphics3D;
using Graphics3D.Graphics3D.Shapes;
using Graphics3D.Graphics3D.Shapes.Geometric;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Изометрическая проекция
namespace Graphics3D
{
    public partial class ExampleForm : Form
    {

        public Scene3D scene;
        public int tmp_X, tmp_Y;
        public bool move;
        public Settings form;

        public ExampleForm()
        {
            InitializeComponent();
            //scene = new Scene3D(pictureBox1);
            //scene.ShowPlanesXY = true;
            //Object3D obj = new Shape3D();
            //scene.AddObject(obj);
            move = false;
            this.MouseWheel += new MouseEventHandler(this_MouseWheel);
            //toolStripStatusLabel2.Text = scene.Alpha.ToString();
            //toolStripStatusLabel4.Text = scene.Betta.ToString();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {           
            //scene.Draw();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            //scene.NewGraphics(pictureBox1);
            //scene.Draw();
            //scene.Resize(pictureBox1.Width, pictureBox1.Height);
            this.Refresh();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            //scene.Draw();
        }


        void this_MouseWheel(object sender, MouseEventArgs e)
        {
            //if (e.Delta > 0)
            //    scene.Scale += 5;
            //else
            //    scene.Scale -= 5;
            //scene.Draw();
            this.Refresh();
        }


        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            tmp_X = e.X;
            tmp_Y = e.Y;
            move = true;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (move)
            {
                scene.Betta = tmp_X - e.X;
                scene.Alpha = tmp_Y - e.Y;

               // scene.Draw();
                this.Refresh();
                //toolStripStatusLabel2.Text = scene.Alpha.ToString();
                //toolStripStatusLabel4.Text = scene.Betta.ToString();
            }

        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (move)
                move = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //scene.NewGraphics(pictureBox1);
            //scene.Draw();
            this.Refresh();
        }

        private void параметрыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (form != null)
            form.Close();
            form = new Settings(this);
            form.Show();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            scene.Alpha = 0;
            scene.Betta = 0;
            //toolStripStatusLabel2.Text = scene.Alpha.ToString();
            //toolStripStatusLabel4.Text = scene.Betta.ToString();
            //scene.Draw();
            this.Refresh();
        }

        private void каркасToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //scene.Draw();
            this.Refresh();
        }

        private void граниToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //scene.Draw();
            this.Refresh();
        }


    }
}

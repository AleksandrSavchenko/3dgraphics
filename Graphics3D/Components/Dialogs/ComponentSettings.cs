﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Graphics3D.Components.Dialogs
{
    public class ComponentSettings
    {
        public Color BackgroundScene { get; set; }
        public bool ShowXYPlane { get; set; }
        public bool ShowYZPlane { get; set; }
        public bool ShowXZPlane { get; set; }
    }
}

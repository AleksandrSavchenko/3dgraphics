﻿namespace Graphics3D.Components.Dialogs
{
    partial class SettingsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageScene = new System.Windows.Forms.TabPage();
            this.btnColor = new System.Windows.Forms.Button();
            this.colorSelect = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.colorDialogBackground = new System.Windows.Forms.ColorDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxXY = new System.Windows.Forms.CheckBox();
            this.checkBoxYZ = new System.Windows.Forms.CheckBox();
            this.checkBoxXZ = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageScene.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 278);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 36);
            this.panel1.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(272, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Ок";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageScene);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(351, 278);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPageScene
            // 
            this.tabPageScene.Controls.Add(this.checkBoxXZ);
            this.tabPageScene.Controls.Add(this.checkBoxYZ);
            this.tabPageScene.Controls.Add(this.checkBoxXY);
            this.tabPageScene.Controls.Add(this.label4);
            this.tabPageScene.Controls.Add(this.label3);
            this.tabPageScene.Controls.Add(this.label2);
            this.tabPageScene.Controls.Add(this.btnColor);
            this.tabPageScene.Controls.Add(this.colorSelect);
            this.tabPageScene.Controls.Add(this.label1);
            this.tabPageScene.Location = new System.Drawing.Point(4, 22);
            this.tabPageScene.Name = "tabPageScene";
            this.tabPageScene.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageScene.Size = new System.Drawing.Size(343, 252);
            this.tabPageScene.TabIndex = 0;
            this.tabPageScene.Text = "Сцена";
            this.tabPageScene.UseVisualStyleBackColor = true;
            // 
            // btnColor
            // 
            this.btnColor.Location = new System.Drawing.Point(108, 11);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(75, 23);
            this.btnColor.TabIndex = 2;
            this.btnColor.Text = "Изменить";
            this.btnColor.UseVisualStyleBackColor = true;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // colorSelect
            // 
            this.colorSelect.Location = new System.Drawing.Point(75, 16);
            this.colorSelect.Name = "colorSelect";
            this.colorSelect.Size = new System.Drawing.Size(16, 16);
            this.colorSelect.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Цвет фона:";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(343, 252);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Плоскость XY:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Плоскость YZ:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Плоскость XZ:";
            // 
            // checkBoxXY
            // 
            this.checkBoxXY.AutoSize = true;
            this.checkBoxXY.Location = new System.Drawing.Point(108, 50);
            this.checkBoxXY.Name = "checkBoxXY";
            this.checkBoxXY.Size = new System.Drawing.Size(89, 17);
            this.checkBoxXY.TabIndex = 7;
            this.checkBoxXY.Text = "Показывать";
            this.checkBoxXY.UseVisualStyleBackColor = true;
            // 
            // checkBoxYZ
            // 
            this.checkBoxYZ.AutoSize = true;
            this.checkBoxYZ.Location = new System.Drawing.Point(108, 74);
            this.checkBoxYZ.Name = "checkBoxYZ";
            this.checkBoxYZ.Size = new System.Drawing.Size(89, 17);
            this.checkBoxYZ.TabIndex = 8;
            this.checkBoxYZ.Text = "Показывать";
            this.checkBoxYZ.UseVisualStyleBackColor = true;
            // 
            // checkBoxXZ
            // 
            this.checkBoxXZ.AutoSize = true;
            this.checkBoxXZ.Location = new System.Drawing.Point(108, 98);
            this.checkBoxXZ.Name = "checkBoxXZ";
            this.checkBoxXZ.Size = new System.Drawing.Size(89, 17);
            this.checkBoxXZ.TabIndex = 9;
            this.checkBoxXZ.Text = "Показывать";
            this.checkBoxXZ.UseVisualStyleBackColor = true;
            // 
            // SettingsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 314);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Name = "SettingsDialog";
            this.Text = "SettingsDialog";
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPageScene.ResumeLayout(false);
            this.tabPageScene.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageScene;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.Panel colorSelect;
        private System.Windows.Forms.ColorDialog colorDialogBackground;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxXZ;
        private System.Windows.Forms.CheckBox checkBoxYZ;
        private System.Windows.Forms.CheckBox checkBoxXY;

    }
}
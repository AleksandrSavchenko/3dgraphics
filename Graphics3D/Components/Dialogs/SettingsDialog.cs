﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Graphics3D.Components.Dialogs
{
    public partial class SettingsDialog : Form
    {
        private ComponentSettings settings;

        public SettingsDialog()
        {
            InitializeComponent();
        }

        public void ShowWithSettings(ComponentSettings set)
        {
            settings = set;
            Init();
            this.ShowDialog();
        }

        public void Init()
        {
            colorSelect.BackColor = settings.BackgroundScene;
            checkBoxXY.Checked = settings.ShowXYPlane;
            checkBoxYZ.Checked = settings.ShowYZPlane;
            checkBoxXZ.Checked = settings.ShowXZPlane;
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            if (colorDialogBackground.ShowDialog() == DialogResult.OK)
            {
                settings.BackgroundScene = colorDialogBackground.Color;
                colorSelect.BackColor = settings.BackgroundScene;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            settings.ShowXYPlane = checkBoxXY.Checked;
            settings.ShowYZPlane = checkBoxYZ.Checked;
            settings.ShowXZPlane = checkBoxXZ.Checked;
            this.Close();
        }

        public ComponentSettings GetSettings()
        {
            return settings;
        }
    }
}

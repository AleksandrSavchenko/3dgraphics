﻿using Graphics3D.Graphics3D;
using Graphics3D.Graphics3D.Shapes.Geometric;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Graphics3D.Components.Dialogs
{
    public partial class FigureDialog : Form
    {
        private FigureType type;
        private Scene3D scene;
        public FigureDialog()
        {
            InitializeComponent();
        }

        public void ShowType(FigureType type, Scene3D scene)
        {
            this.type = type;
            this.scene = scene;
            labelType.Text = getType();
            textBoxName.Text = labelType.Text;
            this.ShowDialog();
        }

        private String getType()
        {
            switch (type)
            {
                case FigureType.OBJECT_3D: return "Объект 3D";
                case FigureType.SHAPE_3D: return "Фигура 3D";
                case FigureType.PLANE_3D: return "Плоскость 3D";
                case FigureType.PYRAMID_3D: return "Пирамида 3D";
                case FigureType.CUBE_3D: return "Куб 3D";
                case FigureType.DODEKAYDER_3D: return "Додекайдер 3D";
                case FigureType.IKOSAYDER_3D: return "Икосайдер 3D";
                default: return "Объект 3D";
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            createObject();
            this.Close();
        }

        private void createObject()
        {
            if (type == FigureType.CUBE_3D)
            {
                Cube3D cube = new Cube3D();
                cube.Name = textBoxName.Text;
                cube.ShiftBy(Convert.ToDouble(textBoxX.Text),Convert.ToDouble(textBoxY.Text),Convert.ToDouble(textBoxZ.Text));
                scene.AddObject(cube);
            }
            if (type == FigureType.PYRAMID_3D)
            {
                Pyramid3D pyramid = new Pyramid3D();
                pyramid.Name = textBoxName.Text;
                pyramid.ShiftBy(Convert.ToDouble(textBoxX.Text), Convert.ToDouble(textBoxY.Text), Convert.ToDouble(textBoxZ.Text));
                scene.AddObject(pyramid);
            }
            if (type == FigureType.DODEKAYDER_3D)
            {
                Dodekayder3D dodekayder = new Dodekayder3D();
                dodekayder.Name = textBoxName.Text;
                dodekayder.ShiftBy(Convert.ToDouble(textBoxX.Text), Convert.ToDouble(textBoxY.Text), Convert.ToDouble(textBoxZ.Text));
                scene.AddObject(dodekayder);
            }
            if (type == FigureType.IKOSAYDER_3D)
            {
                Ikosayder3D ikosayder = new Ikosayder3D();
                ikosayder.Name = textBoxName.Text;
                ikosayder.ShiftBy(Convert.ToDouble(textBoxX.Text), Convert.ToDouble(textBoxY.Text), Convert.ToDouble(textBoxZ.Text));
                scene.AddObject(ikosayder);
            }
        }
    }
}

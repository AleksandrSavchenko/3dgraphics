﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Graphics3D.Graphics3D;
using Graphics3D.Components.Dialogs;

namespace Graphics3D.Components
{
    public partial class Graphic3DTools : UserControl
    {
        protected Scene3D scene;
        protected int tmp_X, tmp_Y;
        protected bool move;

        protected ComponentSettings settings;
        private SettingsDialog settingsDialog = new SettingsDialog();
        private FigureDialog figureDialog = new FigureDialog();

        public Graphic3DTools()
        {
            InitializeComponent();
            scene = new Scene3D(pictureBoxScene);
            scene.Scale = 50;
            scene.Alpha = 45;
            scene.Betta = 60;
            move = false;
            toolStripScale.Text = scene.Scale.ToString();
            this.MouseWheel += new MouseEventHandler(this_MouseWheel);

            settings = new ComponentSettings();
            settings.BackgroundScene = scene.ColorScene;
        }

        private void this_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0)
                scene.Scale += 5;
            else
                scene.Scale -= 5;
            toolStripScale.Text = scene.Scale.ToString();
            scene.Draw();
            this.Refresh();
        }

        private void pictureBoxScene_MouseMove(object sender, MouseEventArgs e)
        {
            if (move)
            {
                scene.Betta = tmp_X - e.X;
                scene.Alpha = tmp_Y - e.Y;

                scene.Draw();
                this.Refresh();
                CoordinatXValue.Text = scene.Alpha.ToString();
                CoordinatYValue.Text = scene.Betta.ToString();
            }
        }

        private void pictureBoxScene_MouseDown(object sender, MouseEventArgs e)
        {
            tmp_X = e.X;
            tmp_Y = e.Y;
            move = true;
        }

        private void pictureBoxScene_Paint(object sender, PaintEventArgs e)
        {
            scene.Draw();
        }

        private void Graphic3DTools_Resize(object sender, EventArgs e)
        {
            scene.Draw();
            scene.Resize(pictureBoxScene.Width, pictureBoxScene.Height);
            this.Refresh();
        }

        private void pictureBoxScene_MouseUp(object sender, MouseEventArgs e)
        {
            if (move)
                move = false;
        }

        private void Graphic3DTools_Load(object sender, EventArgs e)
        {
            scene.Draw();
            this.Refresh();
        }

        public void SaveToFile(String filename)
        {
            scene.SaveToFile(filename);
        }

        public void LoadFromFile(String filename)
        {
            scene = Scene3D.LoadFromFile(filename);
            scene.Draw();
            this.Refresh();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadFromFile(openFileDialog.FileName);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                SaveToFile(saveFileDialog.FileName);
            }
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            settingsDialog.ShowWithSettings(settings);
            settings = settingsDialog.GetSettings();
            scene.ColorScene = settings.BackgroundScene;
            scene.ShowPlanesXY = settings.ShowXYPlane;
            scene.ShowPlanesYZ = settings.ShowYZPlane;
            scene.ShowPlanesZX = settings.ShowXZPlane;
            scene.Draw();
            this.Refresh();
        }

        private void toolBtnObject_Click(object sender, EventArgs e)
        {
            figureDialog.ShowType(FigureType.OBJECT_3D, scene);
            scene.Draw();
            this.Refresh();
        }

        private void toolStripBtnCube_Click(object sender, EventArgs e)
        {
            figureDialog.ShowType(FigureType.CUBE_3D, scene);
            scene.Draw();
            this.Refresh();
        }

        private void toolStripBtnDodekayder_Click(object sender, EventArgs e)
        {
            figureDialog.ShowType(FigureType.DODEKAYDER_3D, scene);
            scene.Draw();
            this.Refresh();
        }

        private void toolStripBtnIcosayder_Click(object sender, EventArgs e)
        {
            figureDialog.ShowType(FigureType.IKOSAYDER_3D, scene);
            scene.Draw();
            this.Refresh();
        }

        private void toolStripBtnPiramid_Click(object sender, EventArgs e)
        {
            figureDialog.ShowType(FigureType.PYRAMID_3D, scene);
            scene.Draw();
            this.Refresh();
        }


    }
}

﻿namespace Graphics3D.Components
{
    partial class Graphic3DTools
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Graphic3DTools));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.CoordinatX = new System.Windows.Forms.ToolStripStatusLabel();
            this.CoordinatXValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.CoordinatY = new System.Windows.Forms.ToolStripStatusLabel();
            this.CoordinatYValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripScale = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolBtnObject = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnShape = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnPiramid = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnCube = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnDodekayder = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnIcosayder = new System.Windows.Forms.ToolStripButton();
            this.panelTools = new System.Windows.Forms.Panel();
            this.pictureBoxScene = new System.Windows.Forms.PictureBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxScene)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CoordinatX,
            this.CoordinatXValue,
            this.CoordinatY,
            this.CoordinatYValue,
            this.toolStripStatusLabel1,
            this.toolStripScale});
            this.statusStrip.Location = new System.Drawing.Point(0, 509);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(878, 22);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip1";
            // 
            // CoordinatX
            // 
            this.CoordinatX.Name = "CoordinatX";
            this.CoordinatX.Size = new System.Drawing.Size(17, 17);
            this.CoordinatX.Text = "X:";
            // 
            // CoordinatXValue
            // 
            this.CoordinatXValue.Name = "CoordinatXValue";
            this.CoordinatXValue.Size = new System.Drawing.Size(13, 17);
            this.CoordinatXValue.Text = "0";
            // 
            // CoordinatY
            // 
            this.CoordinatY.Name = "CoordinatY";
            this.CoordinatY.Size = new System.Drawing.Size(17, 17);
            this.CoordinatY.Text = "Y:";
            // 
            // CoordinatYValue
            // 
            this.CoordinatYValue.Name = "CoordinatYValue";
            this.CoordinatYValue.Size = new System.Drawing.Size(13, 17);
            this.CoordinatYValue.Text = "0";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(56, 17);
            this.toolStripStatusLabel1.Text = "Маштаб:";
            // 
            // toolStripScale
            // 
            this.toolStripScale.Name = "toolStripScale";
            this.toolStripScale.Size = new System.Drawing.Size(13, 17);
            this.toolStripScale.Text = "0";
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBtnObject,
            this.toolStripBtnShape,
            this.toolStripBtnPiramid,
            this.toolStripBtnCube,
            this.toolStripBtnDodekayder,
            this.toolStripBtnIcosayder});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(32, 485);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolBtnObject
            // 
            this.toolBtnObject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnObject.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnObject.Image")));
            this.toolBtnObject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnObject.Name = "toolBtnObject";
            this.toolBtnObject.Size = new System.Drawing.Size(29, 20);
            this.toolBtnObject.Text = "toolStripButton1";
            this.toolBtnObject.ToolTipText = "Добавить объект 3D";
            this.toolBtnObject.Click += new System.EventHandler(this.toolBtnObject_Click);
            // 
            // toolStripBtnShape
            // 
            this.toolStripBtnShape.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBtnShape.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnShape.Image")));
            this.toolStripBtnShape.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnShape.Name = "toolStripBtnShape";
            this.toolStripBtnShape.Size = new System.Drawing.Size(29, 20);
            this.toolStripBtnShape.ToolTipText = "Добавить фигуру 3D";
            // 
            // toolStripBtnPiramid
            // 
            this.toolStripBtnPiramid.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBtnPiramid.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnPiramid.Image")));
            this.toolStripBtnPiramid.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnPiramid.Name = "toolStripBtnPiramid";
            this.toolStripBtnPiramid.Size = new System.Drawing.Size(29, 20);
            this.toolStripBtnPiramid.Text = "Добавить пирамиду 3D";
            this.toolStripBtnPiramid.Click += new System.EventHandler(this.toolStripBtnPiramid_Click);
            // 
            // toolStripBtnCube
            // 
            this.toolStripBtnCube.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBtnCube.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnCube.Image")));
            this.toolStripBtnCube.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnCube.Name = "toolStripBtnCube";
            this.toolStripBtnCube.Size = new System.Drawing.Size(29, 20);
            this.toolStripBtnCube.ToolTipText = "Добавить Куб 3D";
            this.toolStripBtnCube.Click += new System.EventHandler(this.toolStripBtnCube_Click);
            // 
            // toolStripBtnDodekayder
            // 
            this.toolStripBtnDodekayder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBtnDodekayder.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnDodekayder.Image")));
            this.toolStripBtnDodekayder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnDodekayder.Name = "toolStripBtnDodekayder";
            this.toolStripBtnDodekayder.Size = new System.Drawing.Size(29, 20);
            this.toolStripBtnDodekayder.ToolTipText = "Добавить додекайдер 3D";
            this.toolStripBtnDodekayder.Click += new System.EventHandler(this.toolStripBtnDodekayder_Click);
            // 
            // toolStripBtnIcosayder
            // 
            this.toolStripBtnIcosayder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBtnIcosayder.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnIcosayder.Image")));
            this.toolStripBtnIcosayder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnIcosayder.Name = "toolStripBtnIcosayder";
            this.toolStripBtnIcosayder.Size = new System.Drawing.Size(29, 20);
            this.toolStripBtnIcosayder.ToolTipText = "Добавить икосайдер 3D";
            this.toolStripBtnIcosayder.Click += new System.EventHandler(this.toolStripBtnIcosayder_Click);
            // 
            // panelTools
            // 
            this.panelTools.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelTools.Location = new System.Drawing.Point(694, 24);
            this.panelTools.Name = "panelTools";
            this.panelTools.Size = new System.Drawing.Size(184, 485);
            this.panelTools.TabIndex = 2;
            // 
            // pictureBoxScene
            // 
            this.pictureBoxScene.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxScene.Location = new System.Drawing.Point(32, 24);
            this.pictureBoxScene.Name = "pictureBoxScene";
            this.pictureBoxScene.Size = new System.Drawing.Size(662, 485);
            this.pictureBoxScene.TabIndex = 3;
            this.pictureBoxScene.TabStop = false;
            this.pictureBoxScene.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxScene_Paint);
            this.pictureBoxScene.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxScene_MouseDown);
            this.pictureBoxScene.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxScene_MouseMove);
            this.pictureBoxScene.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxScene_MouseUp);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(878, 24);
            this.menuStrip.TabIndex = 4;
            this.menuStrip.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.openToolStripMenuItem.Text = "Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.saveToolStripMenuItem.Text = "Save...";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "3dscene.3dg";
            this.openFileDialog.Filter = "3D Graphics|*.3dg";
            this.openFileDialog.InitialDirectory = ".";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "3D Graphics|*.3dg";
            this.saveFileDialog.InitialDirectory = ".";
            // 
            // Graphic3DTools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBoxScene);
            this.Controls.Add(this.panelTools);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Name = "Graphic3DTools";
            this.Size = new System.Drawing.Size(878, 531);
            this.Load += new System.EventHandler(this.Graphic3DTools_Load);
            this.Resize += new System.EventHandler(this.Graphic3DTools_Resize);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxScene)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.Panel panelTools;
        private System.Windows.Forms.PictureBox pictureBoxScene;
        private System.Windows.Forms.ToolStripStatusLabel CoordinatX;
        private System.Windows.Forms.ToolStripStatusLabel CoordinatXValue;
        private System.Windows.Forms.ToolStripStatusLabel CoordinatY;
        private System.Windows.Forms.ToolStripStatusLabel CoordinatYValue;
        private System.Windows.Forms.ToolStripButton toolBtnObject;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripScale;
        private System.Windows.Forms.ToolStripButton toolStripBtnShape;
        private System.Windows.Forms.ToolStripButton toolStripBtnPiramid;
        private System.Windows.Forms.ToolStripButton toolStripBtnCube;
        private System.Windows.Forms.ToolStripButton toolStripBtnDodekayder;
        private System.Windows.Forms.ToolStripButton toolStripBtnIcosayder;
    }
}

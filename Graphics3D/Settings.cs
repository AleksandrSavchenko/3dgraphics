﻿using Graphics3D.Graphics3D.Shapes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Graphics3D
{
    public partial class Settings : Form
    {
        Shape3D grObject;
        ExampleForm form;

        public Settings(ExampleForm form)
        {
            InitializeComponent();
            grObject = (Shape3D) form.scene.GetObject(0);
            this.form = form;
            trackBar1.Value = form.scene.Scale;
            label1.Text = "Размер: " + form.scene.Scale.ToString();
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            label1.Text = "Размер: " + trackBar1.Value.ToString();
            form.scene.Scale = trackBar1.Value;
            //grObject.Proection();
            form.scene.Draw();
            form.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                grObject.Color = colorDialog1.Color;
                //grObject.Proection();
                form.scene.Draw();
                form.Refresh();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Point3D p = new Point3D();
            p.X = Convert.ToDouble(txtBox_X.Text);
            p.Y = Convert.ToDouble(txtBox_Y.Text);
            p.Z = Convert.ToDouble(txtBox_Z.Text);
            grObject.Add(p);
            //grObject.Proection();
            form.scene.Draw();
            form.Refresh();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            grObject.Clear();
            //grObject.Proection();
            form.scene.Draw();
            form.Refresh();
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D
{
    public enum FigureType
    {
        OBJECT_3D = 0,
        SHAPE_3D = 1,
        PLANE_3D = 2,
        PYRAMID_3D = 3,
        CUBE_3D = 4,
        DODEKAYDER_3D = 5,
        IKOSAYDER_3D = 6
    }
}

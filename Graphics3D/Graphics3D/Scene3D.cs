﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;

namespace Graphics3D.Graphics3D
{
    [Serializable]
    public class Scene3D : Scene3DObjects
    {
        public Scene3D(PictureBox pb) : base(pb)
        {

        }

        public new void Draw()
        {
            base.Draw();
        }

        public new void Refresh()
        {
            base.Refresh();
        }
        public void SaveToFile(String filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Create);
            IFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, this);
            }
            catch (SerializationException ex)
            {
                throw ex;
            }
            finally
            {
                fs.Close();
            }
        }

        public static Scene3D LoadFromFile(String filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open);
            Scene3D obj = null;
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                obj = (Scene3D)formatter.Deserialize(fs);
                return obj;
            }
            catch (SerializationException ex)
            {
                throw ex;
            }
            finally
            {
                fs.Close();
            }
        }
    }
}

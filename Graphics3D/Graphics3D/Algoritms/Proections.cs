﻿using Graphics3D.Graphics3D.Shapes;
using Graphics3D.Graphics3D.Shapes.Geometric;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Algoritms
{
    public static class Proections
    {
        /// <summary>
        /// Строет проекцию для точки
        /// </summary>
        /// <param name="point">точка</param>
        /// <param name="alpha">угол A</param>
        /// <param name="betta">угол B</param>
        /// <returns>Object3D</returns>
        public static Point3D ProectionPoint(Point3D point, double alpha, double betta)
        {
            double[,] mat = new double[3, 1];
            mat[0, 0] = point.X;
            mat[1, 0] = point.Y;
            mat[2, 0] = point.Z;
            double[,] rez = GetIzometrikProection(alpha, betta, mat, 1);
            return new Point3D(rez[0, 0], rez[1, 0], rez[2, 0]);
        }

        /// <summary>
        /// Строет проекцию для точек
        /// </summary>
        /// <param name="points">список точек</param>
        /// <param name="alpha">угол A</param>
        /// <param name="betta">угол B</param>
        /// <returns>Object3D</returns>
        public static List<Point3D> ProectionPoints(List<Point3D> points, double alpha, double betta)
        {
            List<Point3D> list = new List<Point3D>();
            double[,] mat = new double[3, points.Count];
            for (int i = 0; i < points.Count; i++ )
            {
                mat[0, i] = points[i].X;
                mat[1, i] = points[i].Y;
                mat[2, i] = points[i].Z;
            }
            double[,] rez = GetIzometrikProection(alpha, betta, mat, points.Count);

            for (int i = 0; i < points.Count; i++)
            {
                Point3D p = new Point3D(rez[0, i], rez[1, i], rez[2, i]);
                list.Add(p);
            }
            return list;
        }

        /// <summary>
        /// Строет проекцию для объекта
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <param name="alpha">угол A</param>
        /// <param name="betta">угол B</param>
        /// <returns>Object3D</returns>
        public static Object3D ProectionObject3D(Object3D obj, double alpha, double betta)
        {
            Object3D res = obj.Clone();
            List<Point3D> pList = ProectionPoints(res.GetPoints(), alpha, betta);
            res.Clear();
            res.Add(pList);
            return res;
        }

        public static GeometricObject3D ProectionGeometricObject3D(GeometricObject3D obj, double alpha, double betta)
        {
            GeometricObject3D objDo = (GeometricObject3D) obj.Clone();
            GeometricObject3D res = (GeometricObject3D) obj.Clone(); 
            res.Clear();
            res.Add(ProectionPoints(objDo.GetPoints(), alpha, betta));
            res.GetVetges().Clear();
            foreach (Verge v in objDo.GetVetges())
            {
                List<Point3D> pList = ProectionPoints(v.GetPoints(), alpha, betta);
                Verge newVer = new Verge();
                newVer.Add(pList);
                res.GetVetges().Add(newVer);
            }            
            
            return res;
        }


        /// <summary>
        /// Умножение матриц для 3D объектов [n1, 3] x [3, n2]
        /// </summary>
        /// <param name="mat1">матрица1</param>
        /// <param name="n1">количество строк в матрице1</param>
        /// <param name="mat2">матрица2 на которую умножаем</param>
        /// <param name="n2">количество строк в матрице1</param>
        /// <returns>матрица</returns>
        private static double[,] Multiplication(double[,] mat1, int n1, double[,] mat2, int n2)
        {
            double[,] rez = new double[n1, n2];

            for (int i = 0; i < n1; i++)
                for (int j = 0; j < n2; j++)
                {
                    rez[i, j] = 0;
                    for (int z = 0; z < 3; z++)
                        rez[i, j] += mat1[i, z] * mat2[z, j];
                }

            return rez;
        }


        /// <summary>
        /// Матрица ортогонали [0,1]
        /// </summary>
        /// <returns>матрица</returns>
        private static double[,] GetOrtogonaleMatrix()
        {
            double[,] mat = new double[3, 3];
            mat[0, 0] = 1; mat[0, 1] = 0; mat[0, 2] = 0;
            mat[1, 0] = 0; mat[1, 1] = 1; mat[1, 2] = 0;
            mat[2, 0] = 0; mat[2, 1] = 0; mat[2, 2] = 0;
            return mat;
        }

        /// <summary>
        /// Поворот на угол относительно Х
        /// </summary>
        /// <param name="alpha">угол поворота</param>
        /// <returns>матрица</returns>
        private static double[,] GetRotateAMat(double alpha)
        {
            double[,] mat = new double[3, 3];
            mat[0, 0] = 1; mat[0, 1] = 0; mat[0, 2] = 0;
            mat[1, 0] = 0; mat[1, 1] = Math.Cos(alpha * Math.PI / 180); mat[1, 2] = Math.Sin(alpha * Math.PI / 180);
            mat[2, 0] = 0; mat[2, 1] = -Math.Sin(alpha * Math.PI / 180); mat[2, 2] = Math.Cos(alpha * Math.PI / 180);
            return mat;
        }

        /// <summary>
        /// Поворот на угол относительно У
        /// </summary>
        /// <param name="betta">угол поворота</param>
        /// <returns>матрица</returns>
        private static double[,] GetRotateBMat(double betta)
        {
            double[,] mat = new double[3, 3];
            mat[0, 0] = Math.Cos(betta * Math.PI / 180); mat[0, 1] = 0; mat[0, 2] = -Math.Sin(betta * Math.PI / 180);
            mat[1, 0] = 0; mat[1, 1] = 1; mat[1, 2] = 0;
            mat[2, 0] = Math.Sin(betta * Math.PI / 180); mat[2, 1] = 0; mat[2, 2] = Math.Cos(betta * Math.PI / 180);
            return mat;
        }


        /// <summary>
        /// Построение Изометрической проекции
        /// </summary>
        /// <param name="alpha">угол врщения Х</param>
        /// <param name="betta">угол вращения У</param>
        /// <param name="mat">матрица данные которой проецируем</param>
        /// <param name="n">количество точек в матрице</param>
        /// <returns>матрица</returns>
        private static double[,] GetIzometrikProection(double alpha, double betta, double[,] mat, int n)
        {
            // Генерируем матрицу вращения (смещения)
            double[,] matRotat = Multiplication(GetRotateAMat(alpha), 3, GetRotateBMat(betta), 3);
            matRotat = Multiplication(matRotat, 3, mat, n);            
            double[,] rez;
            // Строем ортогональ
            rez = Multiplication(GetOrtogonaleMatrix(), 3, matRotat, n);

            return rez;
        }

        public static List<Point3D> GeneraitMaxPoints(Shape3D obj, int count)
        {
            List<Point3D> list = obj.GetPoints();
            int[] indexmax = new int[count];
            for (int i = 0; i < list.Count; i++)
                indexmax[i] = i;

            double[] znach = new double[obj.GetPoints().Count];
            for (int i = 0; i < list.Count; i++)
                znach[i] = list[i].Y;

            int tmp; double tmpz;
            for (int z = 0; z < list.Count; z++)
                for (int i = 0; i < list.Count-1; i++)
                {
                    if (znach[i] < znach[i + 1])
                    {
                        tmp = indexmax[i];
                        indexmax[i] = indexmax[i + 1];
                        indexmax[i + 1] = tmp;

                        tmpz = znach[i];
                        znach[i] = znach[i + 1];
                        znach[i + 1] = tmpz;
                    }
                }

            List<Point3D> res = new List<Point3D>();
            for(int i=0; i<count; i++)
            {
                res.Add(list[indexmax[i]]);
            }

            return res;
        }
    }
}

﻿using Graphics3D.Graphics3D.Shapes;
using Graphics3D.Graphics3D.Shapes.Geometric;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;

namespace Graphics3D.Graphics3D
{
    [Serializable]
    public abstract class Scene3DObjects
    {
        private List<Object3D> objects;

        protected Bitmap map;
        protected Graphics graphics;
        protected PictureBox holst;

        public bool ShowPlanesXY { get; set; }
        public bool ShowPlanesYZ { get; set; }
        public bool ShowPlanesZX { get; set; }

        protected Plane3D planeXY;
        protected Plane3D planeYZ;
        protected Plane3D planeZX;

        private int planeSize;

        public Scene3DObjects(PictureBox pb)
        {           
            map = new Bitmap(pb.Width, pb.Height);
            graphics = Graphics.FromImage(map);
            objects = new List<Object3D>();
            pb.Image = map;
            holst = pb;
            Init();
            Resize(pb.Width, pb.Height);
        }

        protected void Init()
        {
            ColorScene = Color.Black;
            planeSize = 10;
            planeXY = GeneratePlanesXY();
            planeYZ = GeneratePlanesYZ();
            planeZX = GeneratePlanesZX();
        }

        public void SetHolst(PictureBox pb)
        {
            map = new Bitmap(pb.Width, pb.Height);
            graphics = Graphics.FromImage(map);            
            pb.Image = map;
            holst = pb;
            Resize(pb.Width, pb.Height);
        }

        protected void Init(int newW, int newH)
        {
            if (newW != 0 && newH != 0)
            {                
                map = new Bitmap(newW, newH);
                graphics = Graphics.FromImage(map);
                midle = new Point(newW / 2, newH / 2);
                holst.Image = map;
            }
        }

        /// <summary>
        /// Маштаб сцены
        /// </summary>
        public int Scale { get; set; }

        /// <summary>
        /// Угол A поворота сцены
        /// </summary>
        public double Alpha { get; set; }

        /// <summary>
        /// Угол B поворота сцены
        /// </summary>
        public double Betta { get; set; }

        /// <summary>
        /// Цвет сцены (фона)
        /// </summary>
        public Color ColorScene { get; set; }

        private Point midle = new Point();

        /// <summary>
        /// Обновляет графику сцены
        /// </summary>
        public void Refresh()
        {
            Draw();
        }

        /// <summary>
        /// Отрисовывает объекты сцены
        /// </summary>
        public void Draw()
        {
            ClearImage();
            DrawPlanes();
            foreach (Object3D obj in objects)
            {
                obj.Draw3D(graphics, midle, Scale, Alpha, Betta);
            }           
        }

        public void ClearImage()
        {
            SolidBrush brush = new SolidBrush(ColorScene);
            graphics.FillRectangle(brush, 0, 0, map.Width, map.Height);
        }

        public void AddObject(Object3D obj)
        {
            objects.Add(obj);
        }

        public void AddObjects(List<Object3D> objs)
        {
            foreach (Object3D o in objs)
            objects.Add(o);
        }

        public void ClearObjects()
        {
            objects.Clear();
        }

        public List<Object3D> GetObjects()
        {
            return objects;
        }

        public Object3D GetObject(int index)
        {
            if (index < 0 || index >= objects.Count)
                return null;
            return objects[index];
        }

        public Image GetImage()
        {
            return map;
        }

        public void Resize(int newW, int newH)
        {
            if (map == null)
                return;
            Init(newW, newH);
            midle = new Point(map.Width / 2, map.Height / 2);
            Draw();
        }

        private void DrawPlanes()
        {
            if (ShowPlanesXY)
            {
                planeXY.Draw3D(graphics, midle, Scale, Alpha, Betta);
            }
            if (ShowPlanesYZ)
            {
                planeYZ.Draw3D(graphics, midle, Scale, Alpha, Betta);
            }
            if (ShowPlanesZX)
            {
                planeZX.Draw3D(graphics, midle, Scale, Alpha, Betta);
            }
        }

        protected Plane3D GeneratePlanesXY()
        {
            int step = 3;
            Plane3D o3X = new Plane3D();
            for (int i = -PlaneSize; i <= PlaneSize; i += step)
            {
                for (int j = -PlaneSize; j <= PlaneSize; j += step)
                {
                    Verge v = new Verge();
                    v.Add(new Point3D(i,j,0));
                    v.Add(new Point3D(i + step, j, 0));
                    v.Add(new Point3D(i + step, j + step, 0));
                    v.Add(new Point3D(i, j + step, 0));
                    o3X.Add(v);
                }
            }
            o3X.Color = Color.FromArgb(30, Color.White);
            return o3X;
        }

        protected Plane3D GeneratePlanesYZ()
        {
            int step = 3;
            Plane3D o3X = new Plane3D();
            for (int i = -PlaneSize; i <= PlaneSize; i += step)
            {
                for (int j = -PlaneSize; j <= PlaneSize; j += step)
                {
                    Verge v = new Verge();
                    v.Add(new Point3D(0, i, j));
                    v.Add(new Point3D(0, i + step, j));
                    v.Add(new Point3D(0, i + step, j + step));
                    v.Add(new Point3D(0, i, j + step));
                    o3X.Add(v);
                }
            }
            o3X.Color = Color.FromArgb(30, Color.White);
            return o3X;
        }

        protected Plane3D GeneratePlanesZX()
        {
            int step = 3;
            Plane3D o3X = new Plane3D();
            for (int i = -PlaneSize; i <= PlaneSize; i += step)
            {
                for (int j = -PlaneSize; j <= PlaneSize; j += step)
                {
                    Verge v = new Verge();
                    v.Add(new Point3D(i, 0, j));
                    v.Add(new Point3D(i + step, 0, j));
                    v.Add(new Point3D(i + step, 0, j + step));
                    v.Add(new Point3D(i, 0, j + step));
                    o3X.Add(v);
                }
            }
            o3X.Color = Color.FromArgb(30, Color.White);
            return o3X;
        }

        /// <summary>
        /// Размер сетки плоскости (колличество элементов)
        /// </summary>
        public int PlaneSize 
        {
            get
            {
                return planeSize;
            }
            set 
            {
                planeSize = value;
                planeXY = GeneratePlanesXY();
                planeYZ = GeneratePlanesYZ();
                planeZX = GeneratePlanesZX();
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes
{
    public interface IDraw3D
    {
        void Draw3D(Graphics graphics, Point midle, int Scale, double alpha, double betta);
    }
}

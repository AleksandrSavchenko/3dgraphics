﻿using Graphics3D.Graphics3D.Algoritms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes
{
    [Serializable]
    public class Verge : Shape3D
    {
        public bool Bold { get; set; }
        public override void Draw3D(Graphics graphics, Point midle, int Scale, double alpha, double betta)
        {
            Shape3D obj = this.Clone();
            SetObjectScale(obj, Scale);
            List<Point3D> list = Proections.ProectionPoints(obj.GetPoints(), alpha, betta);
            Point[] res = GetPoints2D(list);
            ShiftOnMid(midle.X, midle.Y, res);
            if (this.Fill)
            {
                SolidBrush brush = new SolidBrush(this.Color);
                graphics.FillPolygon(brush, res);
            }
            else
            {
                Pen p = new Pen(this.Color);
                if (Bold)
                {
                    p.Width = 2;
                }
                graphics.DrawPolygon(p, res);
            }
        }

        public Verge Clone()
        {
            Verge v = new Verge();
            v.Color = this.Color;
            v.Fill = this.Fill;
            
            foreach (Point3D p in this.points)
            {
                Point3D newP = p.Clone();
                v.Add(newP);
            }

            return v;
        }
    }
}

﻿using Graphics3D.Graphics3D.Algoritms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes
{
    /// <summary>
    /// Базовый класс любого 3D объекта
    /// </summary>
    [Serializable]
    public class Shape3D : Object3D
    {
        public Shape3D()
        {
            points = new List<Point3D>();
            this.Color = Color.White;
            this.Fill = false;
        }

        /// <summary>
        /// Отрисовывает объект на сцене
        /// </summary>
        /// <param name="graphics"></param>
        public override void Draw3D(Graphics graphics, Point midle, int Scale, double alpha, double betta)
        {
            if (points.Count == 0)
                return;
            Shape3D obj = (Shape3D) this.Clone();
            SetObjectScale(obj, Scale + this.Scale);
            Shape3D res = (Shape3D)Proections.ProectionObject3D(obj, alpha + this.Alpha, betta + this.Alpha);
            double[,] mat = res.ToMatrix();
            for (int i = 0; i < points.Count; i++)
                for (int j = 0; j < points.Count; j++)
                {
                    Point[] mPoints = ToPoint(midle.X, midle.Y, i, j, mat);
                    graphics.DrawPolygon(new Pen(Color), mPoints);
                }
        }

                
        public override Object3D Clone()
        {
            Shape3D obj = new Shape3D();
            obj.Color = this.Color;
            obj.Fill = this.Fill;

            foreach (Point3D p in points)
                obj.Add(p.Clone());

            return obj;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes
{
    [Serializable]
    public class Point2D
    {
        /// <summary>
        /// Положение точки по оси X
        /// </summary>
        public double X { get; set; }

        /// <summary>
        /// Положение точки по оси Y
        /// </summary>
        public double Y { get; set; }

        public Point2D()
        {
            this.X = this.Y = 0;
        }

        public Point2D(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Устанавливает координаты точки
        /// </summary>
        /// <param name="x">положение точки по оси X</param>
        /// <param name="y">положение точки по оси Y</param>
        /// <param name="z">положение точки по оси Z</param>
        public void SetPoint(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Клонирует объект точки
        /// </summary>
        /// <returns>точка</returns>
        public Point2D Clone()
        {
            return new Point2D(this.X,this.Y);
        }
    }
}

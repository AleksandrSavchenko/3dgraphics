﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes.Geometric
{

    [Serializable]
    public class Plane3D : GeometricObject3D
    {

        public override void Draw3D(Graphics graphics, Point midle, int Scale, double alpha, double betta)
        {
            base.Draw3D(graphics, midle, Scale, alpha, betta);
        }
    }
}

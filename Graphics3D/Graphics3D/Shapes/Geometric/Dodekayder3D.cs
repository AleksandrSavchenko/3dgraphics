﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes.Geometric
{
    [Serializable]
    public class Dodekayder3D : GeometricObject3D
    {
        public Dodekayder3D() : base()
        {
            InitPoints();
            InitVerges();

            SetFill(false);
        }

        public override void Draw3D(Graphics graphics, Point midle, int Scale, double alpha, double betta)
        {
            base.Draw3D(graphics, midle, Scale, alpha, betta);
        }

        private void InitPoints()
        {
            points.Add(new Point3D(0, 0.381966011, 1));
            points.Add(new Point3D(0, -0.381966011, 1));
            points.Add(new Point3D(0, -0.381966011, -1));
            points.Add(new Point3D(0, 0.381966011, -1));

            points.Add(new Point3D(1, 0, 0.381966011));
            points.Add(new Point3D(1, 0, -0.381966011));
            points.Add(new Point3D(-1, 0, -0.381966011));
            points.Add(new Point3D(-1, 0, 0.381966011));

            points.Add(new Point3D(0.381966011, 1, 0));
            points.Add(new Point3D(-0.381966011, 1, 0));
            points.Add(new Point3D(-0.381966011, -1, 0));
            points.Add(new Point3D(0.381966011, -1, 0));

            points.Add(new Point3D(0.618033989, 0.618033989, 0.618033989));
            points.Add(new Point3D(0.618033989, 0.618033989, -0.618033989));
            points.Add(new Point3D(0.618033989, -0.618033989, -0.618033989));
            points.Add(new Point3D(0.618033989, -0.618033989, 0.618033989));

            points.Add(new Point3D(-0.618033989, 0.618033989, 0.618033989));
            points.Add(new Point3D(-0.618033989, 0.618033989, -0.618033989));
            points.Add(new Point3D(-0.618033989, -0.618033989, -0.618033989));
            points.Add(new Point3D(-0.618033989, -0.618033989, 0.618033989));
        }

        private void InitVerges()
        {
            verges.Add(InitVerge(points[0], points[1], points[15], points[4], points[12]));
            verges.Add(InitVerge(points[0], points[1], points[19], points[7], points[16]));
            verges.Add(InitVerge(points[2], points[3], points[13], points[5], points[14]));
            verges.Add(InitVerge(points[2], points[3], points[17], points[6], points[18]));

            verges.Add(InitVerge(points[4], points[5], points[13], points[8], points[12]));
            verges.Add(InitVerge(points[4], points[5], points[14], points[11], points[15]));
            verges.Add(InitVerge(points[6], points[7], points[16], points[9], points[17]));
            verges.Add(InitVerge(points[6], points[7], points[19], points[10], points[18]));

            verges.Add(InitVerge(points[8], points[9], points[16], points[0], points[12]));
            verges.Add(InitVerge(points[8], points[9], points[17], points[3], points[13]));
            verges.Add(InitVerge(points[10], points[11], points[15], points[1], points[19]));
            verges.Add(InitVerge(points[10], points[11], points[14], points[2], points[18]));
        }

        private Verge InitVerge(Point3D p1, Point3D p2, Point3D p3, Point3D p4, Point3D p5)
        {
            Verge v = new Verge();
            v.Add(p1);
            v.Add(p2);
            v.Add(p3);
            v.Add(p4);
            v.Add(p5);
            return v;
        }
    }
}

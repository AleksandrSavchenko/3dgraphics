﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes.Geometric
{
    [Serializable]
    public class Ikosayder3D : GeometricObject3D
    {
        public Ikosayder3D() : base()
        {
            InitPoints();
            InitVerges();

            SetFill(false);
        }

        public override void Draw3D(Graphics graphics, Point midle, int Scale, double alpha, double betta)
        {
            base.Draw3D(graphics, midle, Scale, alpha, betta);
        }

        private void InitPoints()
        {
            points.Add(new Point3D(0, 0.618033989, 1));
            points.Add(new Point3D(0, -0.618033989, 1));
            points.Add(new Point3D(0, -0.618033989, -1));
            points.Add(new Point3D(0, 0.618033989, -1));

            points.Add(new Point3D(1, 0, 0.618033989));
            points.Add(new Point3D(1, 0, -0.618033989));
            points.Add(new Point3D(-1, 0, -0.618033989));
            points.Add(new Point3D(-1, 0, 0.618033989));

            points.Add(new Point3D(0.618033989, 1, 0));
            points.Add(new Point3D(-0.618033989, 1, 0));
            points.Add(new Point3D(-0.618033989, -1, 0));
            points.Add(new Point3D(0.618033989, -1, 0));
        }

        private void InitVerges()
        {
            verges.Add(InitVerge(points[0], points[1], points[4]));
            verges.Add(InitVerge(points[0], points[1], points[7]));
            verges.Add(InitVerge(points[2], points[3], points[5]));
            verges.Add(InitVerge(points[2], points[3], points[6]));

            verges.Add(InitVerge(points[4], points[5], points[8]));
            verges.Add(InitVerge(points[4], points[5], points[11]));
            verges.Add(InitVerge(points[6], points[7], points[9]));
            verges.Add(InitVerge(points[6], points[7], points[10]));

            verges.Add(InitVerge(points[8], points[9], points[0]));
            verges.Add(InitVerge(points[8], points[9], points[3]));
            verges.Add(InitVerge(points[10], points[11], points[1]));            
            verges.Add(InitVerge(points[10], points[11], points[2]));

            verges.Add(InitVerge(points[0], points[4], points[8]));
            verges.Add(InitVerge(points[0], points[7], points[9]));
            verges.Add(InitVerge(points[1], points[4], points[11]));
            verges.Add(InitVerge(points[1], points[7], points[10]));

            verges.Add(InitVerge(points[2], points[5], points[11]));
            verges.Add(InitVerge(points[2], points[6], points[10]));
            verges.Add(InitVerge(points[3], points[5], points[8]));
            verges.Add(InitVerge(points[3], points[6], points[9]));
        }

        private Verge InitVerge(Point3D p1, Point3D p2, Point3D p3)
        {
            Verge v = new Verge();
            v.Add(p1);
            v.Add(p2);
            v.Add(p3);
            return v;
        }
    }
}

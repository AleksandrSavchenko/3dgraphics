﻿using Graphics3D.Graphics3D.Algoritms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes.Geometric
{
    [Serializable]
    public class GeometricObject3D : Object3D 
    {
        protected List<Verge> verges;

        public GeometricObject3D() : base()
        {
            points = new List<Point3D>();
            verges = new List<Verge>();
        }

        public override void Draw3D(Graphics graphics, Point midle, int Scale, double alpha, double betta)
        {
            foreach( Verge verge in verges)
            {
                verge.Draw3D(graphics, midle, Scale + this.Scale, alpha + this.Alpha, betta + this.Betta);
            }
        }

        public bool isThisGran(Verge v, Point3D p)
        {

            foreach (Point3D point3D in v.GetPoints())
            {
                if (point3D.equals(p))
                    return true;
            }
            return false;
        }

        public override Object3D Clone()
        {
            GeometricObject3D obj = new GeometricObject3D();
            obj.Color = this.Color;
            obj.Fill = this.Fill;
            
            foreach(Point3D p in this.points)
            {
                obj.points.Add(p.Clone());
            }

            foreach (Verge v in this.verges)
            {
                obj.verges.Add(v.Clone());
            }

            return obj;
        }

        public List<Verge> GetVetges()
        {
            return verges;
        }

        protected void SetFill()
        {
            foreach (var verg in verges)
                verg.Fill = true;
        }
        protected void SetCascade()
        {
            foreach (var verg in verges)
                verg.Fill = false;
        }

        public void SetFill(bool fill)
        {
            if (fill)
                SetFill();
            else
                SetCascade();
        }

        protected static void SetObjectScale(GeometricObject3D obj, int Scale)
        {
            if (obj.points.Count > 0)
            {
                foreach (Point3D p in obj.points)
                {
                    p.X *= Scale;
                    p.Y *= Scale;
                    p.Z *= Scale;
                }
            }

            if (obj.GetVetges().Count > 0)
            {
                foreach (Verge v in obj.GetVetges())
                {
                    foreach (Point3D p in v.GetPoints())
                    {
                        p.X *= Scale;
                        p.Y *= Scale;
                        p.Z *= Scale;
                    }
                }
            }

        }

        public void Add(Verge v)
        {
            this.verges.Add(v);
        }

        public new Color Color
        {
            get { return base.Color; }
            set
            {
                base.Color = value;
                foreach (Verge v in verges)
                {
                    v.Color = base.Color;
                }
            }
        }



    }
}

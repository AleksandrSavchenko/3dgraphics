﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes.Geometric
{
    [Serializable]
    public class Cube3D : GeometricObject3D
    {
        public Cube3D() : base()
        {
            Verge v1 = new Verge();
            v1.Add(new Point3D(-1, 1, 1));
            v1.Add(new Point3D(1, 1, 1));
            v1.Add(new Point3D(1, 1, -1));
            v1.Add(new Point3D(-1, 1, -1));

            Verge v2 = new Verge();
            v2.Add(new Point3D(1, 1, 1));
            v2.Add(new Point3D(1, -1, 1));
            v2.Add(new Point3D(1, -1, -1));
            v2.Add(new Point3D(1, 1, -1));

            Verge v3 = new Verge();
            v3.Add(new Point3D(-1, -1, 1));
            v3.Add(new Point3D(-1, -1, -1));
            v3.Add(new Point3D(1, -1, -1));
            v3.Add(new Point3D(1, -1, 1));

            Verge v4 = new Verge();
            v4.Add(new Point3D(-1, 1, 1));
            v4.Add(new Point3D(-1, 1, -1));
            v4.Add(new Point3D(-1, -1, -1));
            v4.Add(new Point3D(-1, -1, 1));

            Verge v5 = new Verge();
            v5.Add(new Point3D(-1, 1, -1));
            v5.Add(new Point3D(1, 1, -1));
            v5.Add(new Point3D(1, -1, -1));
            v5.Add(new Point3D(-1, -1, -1));

            Verge v6 = new Verge();
            v6.Add(new Point3D(-1, 1, 1));
            v6.Add(new Point3D(1, 1, 1));
            v6.Add(new Point3D(1, -1, 1));
            v6.Add(new Point3D(-1, -1, 1));

            verges.Add(v1);
            verges.Add(v2);
            verges.Add(v3);
            verges.Add(v4);
            verges.Add(v5);
            verges.Add(v6);

            SetFill(false);
        }

        public override void Draw3D(Graphics graphics, Point midle, int Scale, double alpha, double betta)
        {
            base.Draw3D(graphics, midle, Scale, alpha, betta);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes
{
    [Serializable]
    public abstract class Object3D : IClonableObject3D<Object3D>, IDraw3D
    {
        public String Name { get; set; }
        public double Alpha { get; set; }
        public double Betta { get; set; }
        public int Scale { get; set; }


        protected List<Point3D> points;

        public bool Fill { get; set; }
        public Color Color { get; set; }


        public abstract void Draw3D(Graphics graphics, Point midle, int Scale, double alpha, double betta);

        public abstract Object3D Clone();

        protected static Point[] GetPoints2D(List<Point3D> list)
        {
            List<Point> res = new List<Point>();
            foreach (Point3D p in list)
            {
                res.Add(p.ToPoint());
            }
            return res.ToArray();
        }

        public double[,] ToMatrix()
        {
            double[,] mat = new double[3, points.Count];
            for (int i = 0; i < points.Count; i++)
            {
                mat[0, i] = points[i].X;
                mat[1, i] = points[i].Y;
                mat[2, i] = points[i].Z;
            }
            return mat;
        }


        protected static void SetObjectScale(Shape3D obj, int Scale)
        {
            foreach (Point3D p in obj.points)
            {
                p.X *= Scale;
                p.Y *= Scale;
                p.Z *= Scale;
            }
        }

        protected static Point[] ToPoint(int medium_x, int medium_y, int i, int j, double[,] mat)
        {
            Point[] points = new Point[2];
            points[0].X = medium_x + (int)mat[0, i];
            points[0].Y = medium_y - (int)mat[1, i];
            points[1].X = medium_x + (int)mat[0, j];
            points[1].Y = medium_y - (int)mat[1, j];
            return points;
        }

        protected static void ShiftOnMid(int medium_x, int medium_y, Point[] points)
        {
            for (int i = 0; i < points.Length; i++)
            {
                points[i].X = medium_x + points[i].X;
                points[i].Y = medium_y - points[i].Y;
            }
        }

        /// <summary>
        /// Добавляет точки в объект
        /// </summary>
        /// <param name="points">точки</param>
        public void Add(List<Point3D> points)
        {
            foreach (Point3D p in points)
                this.points.Add(p);
        }

        /// <summary>
        /// Добавляет точку в объект
        /// </summary>
        /// <param name="point">точка</param>
        public void Add(Point3D point)
        {
            points.Add(point);
        }

        /// <summary>
        /// Добавляет точку в объект
        /// </summary>
        /// <param name="x">положение точки по оси X</param>
        /// <param name="y">положение точки по оси Y</param>
        /// <param name="z">положение точки по оси Z</param>
        public void Add(double x, double y, double z)
        {
            Point3D p = new Point3D(x, y, z);
            points.Add(p);
        }

        /// <summary>
        /// Возвращает точку по индексу
        /// </summary>
        /// <param name="index">номер точки в объекте</param>
        /// <returns></returns>
        public Point3D GetPoint(int index)
        {
            if (index >= points.Count || index < 0)
                return null;

            return points[index];
        }

        /// <summary>
        /// Возвращает список точек объекта
        /// </summary>
        /// <returns>список точек</returns>
        public List<Point3D> GetPoints()
        {
            return points;
        }

        /// <summary>
        /// Очищает список точек объекта
        /// </summary>
        public void Clear()
        {
            points.Clear();
        }

        public void ShiftByX(double shift)
        {
            foreach (Point3D p in points)
            {
                p.X += shift;
            }
        }

        public void ShiftByY(double shift)
        {
            foreach (Point3D p in points)
            {
                p.Y += shift;
            }
        }
        public void ShiftByZ(double shift)
        {
            foreach (Point3D p in points)
            {
                p.Z += shift;
            }
        }

        public void ShiftBy(double shiftX, double shiftY, double shiftZ)
        {
            ShiftByX(shiftX);
            ShiftByY(shiftY);
            ShiftByZ(shiftZ);
        }

    }
}

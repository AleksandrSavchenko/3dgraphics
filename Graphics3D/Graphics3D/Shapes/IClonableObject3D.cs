﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes
{
    public interface IClonableObject3D<T>
    {
        T Clone();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Graphics3D.Graphics3D.Shapes
{
    [Serializable]
    public class Point3D
    {
        /// <summary>
        /// Положение точки по оси X
        /// </summary>
        public double X { get; set; }

        /// <summary>
        /// Положение точки по оси Y
        /// </summary>
        public double Y { get; set; }

        /// <summary>
        /// Положение точки по оси Z
        /// </summary>
        public double Z { get; set; }

        public Point3D()
        {
            this.X = this.Y = this.Z = 0;
        }

        public Point3D(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        /// <summary>
        /// Устанавливает координаты точки
        /// </summary>
        /// <param name="x">положение точки по оси X</param>
        /// <param name="y">положение точки по оси Y</param>
        /// <param name="z">положение точки по оси Z</param>
        public void SetPoint(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public Point ToPoint()
        {
            return new Point((int)X, (int)Y);
        }

        /// <summary>
        /// Клонирует объект точки
        /// </summary>
        /// <returns>точка</returns>
        public Point3D Clone()
        {
            return new Point3D(this.X,this.Y, this.Z);
        }

        public bool equals(Point3D point3D) 
        {
            if (this.X == point3D.X && this.Y == point3D.Y && this.Z == point3D.Z)
            {
                return true;
            }
            return false;
        }
    }
}
